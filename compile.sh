#!/bin/bash

git clone --recursive https://github.com/gnuradio/gnuradio.git

cd gnuradio

cnt=`grep 'processor.*:' /proc/cpuinfo|wc -l`
cnt=`expr $cnt - 1`
if [ $cnt -lt 1 ]
then
  cnt=1
fi

mkdir build
cd build

cmake \
  -DPythonLibs_FIND_VERSION:STRING="2.7" \
  -DPythonInterp_FIND_VERSION:STRING="2.7" ../

make -j$cnt

make install

echo "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib" >> ~/.bashrc && \
echo "export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local/lib/pkgconfig" >> ~/.bashrc

rm -rf /opt/gnuradio
