#https://gnuradio.org/redmine/projects/gnuradio/wiki/UbuntuInstall

FROM ubuntu:14.04

MAINTAINER Frederik Granna

RUN apt-get update && apt-get dist-upgrade -yf  && \
    apt-get install -y git-core cmake g++ python-dev swig \
pkg-config libfftw3-dev libboost1.55-all-dev libcppunit-dev libgsl0-dev \
libusb-dev libsdl1.2-dev python-wxgtk2.8 python-numpy \
python-cheetah python-lxml doxygen libxi-dev python-sip \
libqt4-opengl-dev libqwt-dev libfontconfig1-dev libxrender-dev \
python-sip python-qt4 python-sip-dev python-gtk2-dev && \
    apt-get clean && apt-get autoremove && \
    rm -rf /var/lib/apt/lists/*

RUN echo "export PYTHONPATH=/usr/local/lib/python2.7/dist-packages" > ~/.bashrc

WORKDIR /opt

COPY compile.sh /opt/compile.sh

RUN ./compile.sh

WORKDIR /
